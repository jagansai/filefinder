package com.utils

data class Args
(
        var path: String = "",
        var pattern: String = "",
        var filter: String = "",
        var case: Boolean = true,
        var numLines : Int = 10
)