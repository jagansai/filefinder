package com.utils

import java.nio.file.Path

class StringUtils {
    companion object {
        @JvmStatic
        fun contains(text: String, substr: String, ignoreCase: Boolean): Boolean {
            return text.contains(substr, ignoreCase)
        }

        @JvmStatic
        fun sameFileType(file: Path, extn: String): Boolean {
            val fileName = file.fileName.toString()
            return fileName.endsWith(extn, true)
        }
    }
}