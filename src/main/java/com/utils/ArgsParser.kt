package com.utils

import io.vavr.control.Either
import java.util.function.Consumer

object ArgsParser {
    private enum class TypeOfArgs {
        StringOptions,
        NumOptions,
        Flags,
        Nothing
    }

    fun parseArgs(splitStrings: Array<String>): Args {
        val args = Args()
        val takePath = Consumer<Either<String, Int>> { args.path = (it.left) }
        val takeFilter = Consumer<Either<String, Int>> { args.filter = (it.left) }
        val takeCase =  Consumer<Either<String, Int>> { args.case = true }
        val takePattern = Consumer<Either<String, Int>> { args.pattern = (it.left) }
        val takeNum = Consumer<Either<String, Int>> { args.numLines = (it.get()) }

        val stringConsumerMap = mutableMapOf(
                "-ic" to takeCase,
                "-Path" to takePath,
                "-Filter" to takeFilter,
                "-Pattern" to takePattern,
                "-Num" to takeNum)

        val consumerTypeOfArgsMap = mutableMapOf(
                takeCase to  TypeOfArgs.Flags,
                takeFilter to TypeOfArgs.StringOptions,
                takePath to TypeOfArgs.StringOptions,
                takePattern to TypeOfArgs.StringOptions,
                takeNum to TypeOfArgs.NumOptions
        )

        var index = 0
        while (index < splitStrings.size) {
            val stringConsumer = stringConsumerMap[splitStrings[index]]
            val typeOfArgs = consumerTypeOfArgsMap[stringConsumer]
            if (typeOfArgs != null && typeOfArgs != TypeOfArgs.Nothing) {
                index += when (typeOfArgs) {
                    TypeOfArgs.StringOptions -> {
                        stringConsumer?.accept(Either.left(splitStrings[index + 1]))
                        2
                    }
                    TypeOfArgs.NumOptions -> {
                        stringConsumer?.accept(Either.right(splitStrings[index + 1].toInt()))
                        2
                    }
                    TypeOfArgs.Flags -> {
                        stringConsumer?.accept(Either.left(splitStrings[index]))
                        1
                    }
                    TypeOfArgs.Nothing -> 1
                }
            }
        }
        return args
    }
}
