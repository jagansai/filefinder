package com.callable

import com.utils.StringUtils
import java.io.IOException
import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes
import kotlin.streams.asSequence

class FinderVisitor(private val fileExtn: String, private val pattern: String, private var numLines :Int = 5) :
        SimpleFileVisitor<Path>() {

    @Throws(IOException::class)
    override fun visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult {
        find(file)
        return if (numLines == 0)
            FileVisitResult.TERMINATE
        else
            FileVisitResult.CONTINUE
    }

    @Throws(IOException::class)
    override fun visitFileFailed(file: Path, exc: IOException?): FileVisitResult {
        return super.visitFileFailed(file, exc)
    }

    private fun find(file: Path) {
        if (!StringUtils.sameFileType(file, fileExtn)) return
        try {
            val enumerate = generateSequence(1) { it + 1 }
            val results = Files.lines(file).asSequence().zip(enumerate)
                .filter { (line, _) -> StringUtils.contains(line, pattern, true) }
                .take(numLines)
                .map { (line, lineNum) -> String.format("%s:%d", line, lineNum) }
                .toList()
            numLines -= results.count()
            if (results.count() > 0) {
                println("$file -> ")
                results.forEach { println(it) }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}
