package com.callable;

import com.utils.Args;
import com.utils.ArgsParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/*
*** The class is used as a tool to find files given the pattern.
* * For now this class takes pattern and searches for the pattern in the list of files
* * that has the extension.
* * Improvement:
* *   1) Take the argument with which we can decide to use to regex or simple match.
* *   2) Take the argument to show only files or details of the search.
 */

public class Finder {

    public static void FindPattern(String[] params) {
        try {
            final Args args = ArgsParser.INSTANCE.parseArgs(params);
            final var finderVisitor = new FinderVisitor(args.getFilter(), args.getPattern(), args.getNumLines());
            Files.walkFileTree(Path.of(args.getPath()), finderVisitor);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
