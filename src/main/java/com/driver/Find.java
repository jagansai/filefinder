

package com.driver;


import com.callable.Finder;

class Find {
    public static void main(String[] args) {
        if (args.length == 0)
            System.err.println("zero arguments");
        else
            Finder.FindPattern(args);
    }
}